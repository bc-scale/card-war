from actors import Deck, Player


def play_war():
    # Game setup - START
    player_one = Player('John')
    player_two = Player('Jane')

    new_deck = Deck()
    new_deck.shuffle()

    for x in range(26):
        player_one.add_cards(new_deck.deal_a_card())
        player_two.add_cards(new_deck.deal_a_card())
    # Game setup - END

    # Game logic - START
    game_on = True
    rounds_count = 0
    capacity = 5

    while game_on:
        rounds_count += 1
        print(f'Round {rounds_count}')

        # Check if Player 1 ran out of cards
        if len(player_one.all_cards) == 0:
            print(f'{player_one.name} has no more cards. {player_two.name} is the winner!')
            game_on = False
            break

        # Check if Player 2 ran out of cards
        if len(player_two.all_cards) == 0:
            print(f'{player_two.name} has no more cards. {player_one.name} is the winner!')
            game_on = False
            break

        # Start a round
        player_one_cards_on_desk = []
        player_one_cards_on_desk.append(player_one.remove_card())

        player_two_cards_on_desk = []
        player_two_cards_on_desk.append(player_two.remove_card())

        # Checks on desk
        # Assuming the war is on
        at_war = True

        while at_war:
            # Player's 1 card is greater than Player's 2 card
            if player_one_cards_on_desk[-1].value > player_two_cards_on_desk[-1].value:
                player_one.add_cards(player_one_cards_on_desk)
                player_one.add_cards(player_two_cards_on_desk)
                at_war = False

            # Player's 2 card is greater than Player's 1 card
            elif player_one_cards_on_desk[-1].value < player_two_cards_on_desk[-1].value:
                player_two.add_cards(player_one_cards_on_desk)
                player_two.add_cards(player_two_cards_on_desk)
                at_war = False

            # Players' cards are equal in value - it's a WAR!
            else:
                print('It\'s a WAR!')

                # Check whether Player 1 has enough cards to go to WAR
                if len(player_one.all_cards) < capacity:
                    print(f'{player_one.name} has not enough cards to go to WAR')
                    print(f'{player_two.name} WINs')
                    game_on = False
                    break
                # Check whether Player 2 has enough cards to go to WAR
                elif len(player_two.all_cards) < capacity:
                    print(f'{player_two.name} has not enough cards to go to WAR')
                    print(f'{player_one.name} WINs')
                    game_on = False
                    break
                # Draw cards for the WAR
                else:
                    for num in range(capacity):
                        player_one_cards_on_desk.append(player_one.remove_card())
                        player_two_cards_on_desk.append(player_two.remove_card())
    # Game logic - END


if __name__ == "__main__":
    play_war()
