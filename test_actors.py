import unittest
from actors import Card, Deck, Player


class TestCardInit(unittest.TestCase):

    def setUp(self):
        suit = 'Clubs'
        rank = 'Three'
        self.new_card = Card(suit, rank)

    def test_card_init_str(self):
        self.assertEqual('Three of Clubs', str(self.new_card))

    def test_card_init_suit(self):
        self.assertEqual('Clubs', self.new_card.suit)

    def test_card_init_rank(self):
        self.assertEqual('Three', self.new_card.rank)

    def test_card_init_value(self):
        self.assertEqual(3, self.new_card.value)


class TestDeckInit(unittest.TestCase):

    def setUp(self):
        self.regular_deck = ['Two of Hearts', 'Three of Hearts', 'Four of Hearts', 'Five of Hearts', 'Six of Hearts', 'Seven of Hearts', 'Eight of Hearts', 'Nine of Hearts', 'Ten of Hearts', 'Jack of Hearts', 'Queen of Hearts', 'King of Hearts', 'Ace of Hearts', 'Two of Diamonds', 'Three of Diamonds', 'Four of Diamonds', 'Five of Diamonds', 'Six of Diamonds', 'Seven of Diamonds', 'Eight of Diamonds', 'Nine of Diamonds', 'Ten of Diamonds', 'Jack of Diamonds', 'Queen of Diamonds', 'King of Diamonds', 'Ace of Diamonds', 'Two of Spades', 'Three of Spades', 'Four of Spades', 'Five of Spades', 'Six of Spades', 'Seven of Spades', 'Eight of Spades', 'Nine of Spades', 'Ten of Spades', 'Jack of Spades', 'Queen of Spades', 'King of Spades', 'Ace of Spades', 'Two of Clubs', 'Three of Clubs', 'Four of Clubs', 'Five of Clubs', 'Six of Clubs', 'Seven of Clubs', 'Eight of Clubs', 'Nine of Clubs', 'Ten of Clubs', 'Jack of Clubs', 'Queen of Clubs', 'King of Clubs', 'Ace of Clubs']

    def test_deck_init(self):
        a_deck = Deck()
        cards_list = []
        for a_card in a_deck.all_cards:
            cards_list.append(str(a_card))
        self.assertEqual(self.regular_deck, cards_list)

    def test_deck_shuffle(self):
        a_deck = Deck()
        a_deck.shuffle()
        shuffled_cards_list = []
        for a_card in a_deck.all_cards:
            shuffled_cards_list.append(str(a_card))
        self.assertNotEqual(self.regular_deck, shuffled_cards_list)

    def test_deal_from_deck(self):
        a_deck = Deck()
        a_deck.shuffle()
        a_card = a_deck.deal_a_card()
        self.assertIsNotNone(a_card)
        self.assertEqual(51, len(a_deck.all_cards))


class TestPlayerInit(unittest.TestCase):

    def setUp(self):
        self.a_player = Player('Unit')

    def test_player_init(self):
        self.assertEqual(0, len(self.a_player.all_cards))
        self.assertEqual('Unit', self.a_player.name)
        self.assertEqual('Player Unit has 0 cards.', str(self.a_player))

    def test_player_add_single_card(self):
        new_card = Card('Spades', 'Two')
        self.a_player.add_cards(new_card)
        self.assertEqual('Player Unit has 1 cards.', str(self.a_player))

    def test_player_add_multiple_cards(self):
        new_cards = [Card('Spades', 'Two'), Card('Spades', 'Six'), Card('Clubs', 'Three')]
        self.a_player.add_cards(new_cards)
        self.assertEqual('Player Unit has 3 cards.', str(self.a_player))

    def test_player_remove_a_card(self):
        new_cards = [Card('Spades', 'Two'), Card('Spades', 'Six'), Card('Clubs', 'Three')]
        self.a_player.add_cards(new_cards)
        self.a_player.remove_card()
        self.assertEqual('Player Unit has 2 cards.', str(self.a_player))


if __name__ == '__main__':
    unittest.main()
