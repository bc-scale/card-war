import random

suits = ('Hearts', 'Diamonds', 'Spades', 'Clubs')
ranks = ('Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King', 'Ace')
values = {'Two': 2, 'Three': 3, 'Four': 4, 'Five': 5, 'Six': 6, 'Seven': 7, 'Eight': 8,
          'Nine': 9, 'Ten': 10, 'Jack': 11, 'Queen': 12, 'King': 13, 'Ace': 14}


class Card:

    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank
        self.value = values[rank]

    def __str__(self):
        return self.rank + " of " + self.suit


class Deck:

    def __init__(self):
        self.all_cards = []
        for suite in suits:
            for rank in ranks:
                a_new_card = Card(suite, rank)
                self.all_cards.append(a_new_card)

    def shuffle(self):

        random.shuffle(self.all_cards)

    def deal_a_card(self):

        return self.all_cards.pop()


class Player:

    def __init__(self, name):
        self.name = name
        self.all_cards = []

    def remove_card(self):
        # Removes a card from the top of the deck on hand
        return self.all_cards.pop(0)

    def add_cards(self, new_cards):
        if type([]) == type(new_cards):
            # Extends the end of the deck on hand by new CARDS
            self.all_cards.extend(new_cards)
        else:
            # Appends the end of the deck on hand by new CARD
            self.all_cards.append(new_cards)

    def __str__(self):
        return f'Player {self.name} has {len(self.all_cards)} cards.'
